package pxcltime

import (
	"errors"
	"math"
	"strconv"
	"time"
)

const pxDayConv = 2.125597415

var pDay0 = Pxtime{
	EarthTime:   day0,
	Day:         1,
	Month:       1,
	Year:        15000,
	YearDay:     1,
	Millisecond: 0,
	Second:      0,
	Minute:      0,
	Hour:        0,
}

var pDay0Days = pDay0.Year * 294

// TimeToPxtime - Convert an Earth time to a Parallax Ptime
func TimeToPxtime(t time.Time) (Pxtime, error) {
	eDays := t.Sub(day0).Hours() / 24
	pDays := float64(eDays) / pxDayConv
	totalDays := float64(pDay0Days) + pDays
	pYears := math.Floor(math.Floor(pDays) / 294)
	pYear := int(float64(pDay0.Year) + pYears)
	closestLeapYear := closestNumber(pYear, 3)
	leapYear := closestLeapYear == pYear
	closLeapYearDays := closestLeapYear * 294

	pYearDay := totalDays - float64(closLeapYearDays)

	var pYearMonths, pYearDays int
	if leapYear {
		pYearMonths = 17
		pYearDays = 306
	} else {
		pYearMonths = 16
		pYearDays = 288
		pYearDay -= 306
	}

	if pYear-closestLeapYear == 2 {
		pYearDay -= 288
	}

	pMonth := (int(math.Floor(pYearDay)) / 18) + 1

	pDay := math.Mod(pYearDay, 18.0) + 1

	pHours := 36 * getDecimal(pYearDay)
	pHour := int(math.Floor(pHours))
	pMinutes := 100 * getDecimal(pHours)
	pMinute := int(math.Floor(pMinutes))
	pSeconds := 100 * getDecimal(pMinutes)
	pSecond := int(math.Floor(pSeconds))
	pMilliseconds := 1000 * getDecimal(pSeconds)
	pMillisecond := int(math.Floor(pMilliseconds))

	if pMonth > pYearMonths {
		return *new(Pxtime), errors.New("Math error: Too many months")
	} else if int(math.Floor(pYearDay)) > pYearDays {
		return *new(Pxtime), errors.New("Math error: Too high of a YearDay")
	} else if int(math.Floor(pDay)) > 16 {
		return *new(Pxtime), errors.New("Math error: Too high of a MonthDay")
	} else {
		return Pxtime{
			EarthTime:   t,
			Day:         int(math.Floor(pDay)),
			Month:       pMonth,
			Year:        pYear,
			YearDay:     int(math.Floor(pYearDay)),
			Millisecond: pMillisecond,
			Second:      pSecond,
			Minute:      pMinute,
			Hour:        pHour,
		}, nil
	}
}

// PxDate - Get Pxtime from a specified Parallax Date
func PxDate(day int, month int, year int) (Pxtime, error) {
	leapYear := math.Mod(float64(year), 3) == 0.0
	if day > 18 || day <= 0 {
		return *new(Pxtime), errors.New("Invalid Day: " + strconv.Itoa(day))
	} else if (leapYear && month > 17) || (!leapYear && month > 16) || month <= 0 {
		return *new(Pxtime), errors.New("Invalid Month: " + strconv.Itoa(month))
	} else if year < 0 {
		return *new(Pxtime), errors.New("Invalid Year: " + strconv.Itoa(year))
	} else {
		yearDay := ((month - 1) * 18) + day
		return Pxtime{
			EarthTime:   *new(time.Time),
			Day:         day,
			Month:       month,
			Year:        year,
			YearDay:     yearDay,
			Millisecond: 0,
			Second:      0,
			Minute:      0,
			Hour:        0,
		}, nil
	}
}

// Pxtime - A Parallax Time object
type Pxtime struct {
	EarthTime   time.Time
	Day         int
	Month       int
	Year        int
	YearDay     int
	Millisecond int
	Second      int
	Minute      int
	Hour        int
}
