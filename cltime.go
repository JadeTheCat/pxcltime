package pxcltime

import (
	"errors"
	"math"
	"strconv"
	"time"
)

const clDayConv = 2.12319017

var cDay0 = Cltime{
	EarthTime:   day0,
	Day:         1,
	Month:       1,
	Year:        15000,
	YearDay:     1,
	Millisecond: 0,
	Second:      0,
	Minute:      0,
	Hour:        0,
}

var cDay0Days = cDay0.Year * (294 + (1 / 3))

// TimeToCltime - Convert an Earth time to a Parallax Ptime
func TimeToCltime(t time.Time) (Cltime, error) {
	eDays := t.Sub(day0).Hours() / 24
	cDays := eDays / clDayConv
	cYears := int(math.Floor(cDays / (294 + (1 / 3))))
	cYear := cDay0.Year + cYears
	totalDays := float64(cDay0Days) + cDays
	totalYearDays := cYear * (294 + (1 / 3))
	cYearDay := (int(math.Floor(totalDays)) - totalYearDays) + 1

	cMonth := cYearDay / 18

	cDay := int(math.Mod(float64(cYearDay), 18.0)) + 1

	cHours := 36 * getDecimal(totalDays)
	cHour := int(math.Floor(cHours))
	cMinutes := 100 * getDecimal(cHours)
	cMinute := int(math.Floor(cMinutes))
	cSeconds := 100 * getDecimal(cMinutes)
	cSecond := int(math.Floor(cSeconds))
	cMilliseconds := 1000 * getDecimal(cSeconds)
	cMillisecond := int(math.Floor(cMilliseconds))

	var cYearDays int
	if int(math.Mod(float64(cYear), 3.0)) == 0 {
		cYearDays = 295
	} else {
		cYearDays = 294
	}

	if cMonth == 17 && (cYearDays == 295 && cDay > 7) {
		return *new(Cltime), errors.New("Intercalary Overflow")
	} else if cMonth == 17 && (cYearDays == 294 && cDay > 6) {
		return *new(Cltime), errors.New("Intercalary Overflow")
	} else if cMonth != 17 && cDay > 18 {
		return *new(Cltime), errors.New("Month Overflow")
	} else if cDay <= 0 || cMonth <= 0 || cYear < 0 {
		return *new(Cltime), errors.New("Underflow")
	} else {
		return Cltime{
			EarthTime:   t,
			Day:         cDay,
			Month:       cMonth,
			Year:        cYear,
			YearDay:     cYearDay,
			Millisecond: cMillisecond,
			Second:      cSecond,
			Minute:      cMinute,
			Hour:        cHour,
		}, nil
	}

}

// ClDate - Get Cltime from a specified Parallax Date
func ClDate(day int, month int, year int) (Cltime, error) {
	leapYear := math.Mod(float64(year), 3) == 0.0
	if day > 18 || day <= 0 {
		return *new(Cltime), errors.New("Invalid Day: " + strconv.Itoa(day))
	} else if (leapYear && month > 17) || (!leapYear && month > 16) || month <= 0 {
		return *new(Cltime), errors.New("Invalid Month: " + strconv.Itoa(month))
	} else if year < 0 {
		return *new(Cltime), errors.New("Invalid Year: " + strconv.Itoa(year))
	} else {
		yearDay := ((month - 1) * 18) + day
		return Cltime{
			EarthTime:   *new(time.Time),
			Day:         day,
			Month:       month,
			Year:        year,
			YearDay:     yearDay,
			Millisecond: 0,
			Second:      0,
			Minute:      0,
			Hour:        0,
		}, nil
	}
}

// Cltime - A Parallax Time object
type Cltime struct {
	EarthTime   time.Time
	Day         int
	Month       int
	Year        int
	YearDay     int
	Millisecond int
	Second      int
	Minute      int
	Hour        int
}
