package pxcltime

import (
	"math"
	"time"
)

var day0 = time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC)

func getDecimal(f float64) float64 {
	return f - math.Floor(f)
}
func closestNumber(n, m int) int {
	var q, n1 int
	q = int(n / m)
	n1 = m * q
	return n1
}
